import {Component, OnInit} from '@angular/core';
import {CardService} from './card/card.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'card-app';
  public listCards: any[];

  constructor(private cardService: CardService) {
    this.listCards = [];
  }

  ngOnInit() {
    this.addContentCard();
  }

  public addContentCard(): void {
    this.listCards = this.cardService.getContentCard;
  }
}
